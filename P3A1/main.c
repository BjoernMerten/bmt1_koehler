#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int is_kkn(char Karten[]);

int kk_number(char Karten[], int laenge);

int kk_holder(char Karten[], int k[][4], int zeilen, int laenge);

int main (){
    int K[][4] = { 
        {51,        16,      2,       0},
        {52,        16,      2,       0},
        {53,        16,      2,       0},
        {54,        16,      2,       0},
        {55,        16,      2,       0},
        {4,         13,      1,       1},
        {4,         16,      1,       1},
        {34,        15,      2,       2},
        {37,        15,      2,       2},
        {300,       14,      3,       3},
        {301,       14,      3,       3},
        {302,       14,      3,       3},
        {303,       14,      3,       3},
        {304,       14,      3,       3},
        {305,       14,      3,       3},
        {36,        14,      2,       3},
        {38,        14,      2,       3},
        {6011,      16,      4,        4},
        {2014,      15,      4,       5},
        {2149,      15,      4,       5},
        {3,         16,      1,       6},
        {2131,      15,      4,       6},
        {1800,      15,      4,       6},
    };
    char A[][20]={
        {"MASTER"},
        {"VISA"},
        {"AMEX"},
        {"DINERS"},
        {"DISCOVER"},
        {"enRoute"},
        {"JCB"},
    };
    int run=0,holder=0,z=23,l=0;
char Karte[21]="                     ";

printf("Bitte Kartennummer eingeben\n");
fgets(Karte,21,stdin);

if(is_kkn(Karte)==1)
{
    if(kk_number(Karte,strlen(Karte))==1)
    {
        printf("Kartennummer ist gueltig!\n");
    }
    else 
        {
        printf("Kartennummer ungueltig\n");
        return(0);
        }
}
else 
        {       
        printf("ungueltige Eingabe\n");
        return(0);
        }
l=strlen(Karte);
holder=kk_holder(Karte,K,z,l);
if(holder==0)
{
    printf("Karte nicht bekannt");
}
else
    printf("Karte wird herausgegeben von: %s", A[holder]);
}

int is_kkn(char Karten[]){
    char *p;
    p=Karten;
    int c=1;
    
    do{
    if (*p == '\n')
        *p = '\0';
        p++;
        }while (*p != '\0');
        if (strlen(Karten)<10 | strlen(Karten)>20)
            c=0;
    p=Karten;
    do{ 
        if (*p < 48 | *p > 57)
            c=0;
            p++;
        }while (*p != '\0');
        return (c);
}

int kk_number(char Karten[], int laenge){
    int offset=0;
    int sum=0;
    int ziffer=0;
    char mem[20]="00000000000000000000";
    //strncpy(mem,Karten,laenge);
    
    offset=20-strlen(Karten);
    for(int i=offset;i<20;i++)
    { 
        mem[i]=Karten[i-offset];
    }
    for(int i=19;i>=0;i--)
    {
        ziffer=mem[i]-48;
        if(i%2==0)
        {ziffer *= 2;
        if (ziffer > 9)
            ziffer -= 9;
        }
        sum+=ziffer;
    }
    if (sum % 10 == 0 && sum != 0)
        return (1);
    else
        return (0);  
}

int kk_holder(char Karten[], int k[][4], int zeilen, int laenge){
    char mem[20]="00000000000000000000";
    for(int i=0; i<zeilen; i++){
        strcpy(mem,Karten);
        mem[k[i][2]]='\0';
        if(atoi(mem)==k[i][0] && laenge==k[i][1])
            return (k[i][3]);
        }
    return(0);
    
}