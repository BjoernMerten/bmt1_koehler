#include <stdio.h>
#include <stdlib.h>

int main() 
{
    float a=0.0;
    
    while (a <= 40)
    {
        printf("%5.2f Grad Celsius sind %6.2f Grad Fahrenheit\n", a, ((a*1.8)+32));
        a += 5;
    }
    return ();
}

