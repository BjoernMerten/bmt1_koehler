#include <stdio.h>
#include <stdlib.h>

int Integer();
float Floater();
double Double();


int main() 
{
    printf("Die erwartete Zahl fuer alle Rechnungen ist 0.\n\n");
    
    printf("Die Zahl fuer die Integer-Berechnung ist %i.\n\n", Integer());
    
    printf("Die Zahl fuer die Float-Berechnung ist %f.\n\n", Floater());
    
    printf("Die Zahl fuer die Double-Berechnung ist %f.\n\n", Double());

    return (0);
}

int Integer()
{
    int a = 23;
    int b = 42;
    int c = 2073741824;
    int d = -2073741714;
    int e = -64;
    int f = -111;
    
    return(a+b+c+d+e+f);
}

float Floater()
{
    float a = 23;
    float b = 42;
    float c = 2073741824;
    float d = -2073741714;
    float e = -64;
    float f = -111;
    
    return(a+b+c+d+e+f);
}

double Double()
{
    double a = 23;
    double b = 42;
    double c = 2073741824;
    double d = -2073741714;
    double e = -64;
    double f = -111;
    
    return(a+b+c+d+e+f);
}