#include<stdio.h>
#include<string.h>
#include<stdlib.h>

/*
	Dieses Programm demonstriert verschiedene Möglichkeiten 
	zur "sicheren" Eingabe von Strings in C.

	Es sind verschiedene Funktionen angegeben, die von Schritt zu Schritt 
	immer "sicherer" und mächtiger bzw. flexibler werden. Die jeweiligen 
	Vor- und Nachteile sind in Kommentaren erläutert.

	In allen Funktionen steht s für einen char-Zeiger auf den Speicherbereich,
	in den der String geschrieben werden soll. Wenn s als Parameter übergeben
	wird, muß vorher hinreichender Platz reserviert werden, d.h. s muß auf
	einen Speicherbereich zeigen, in dem der String gefahrlos angelegt werden
	kann. Die letzten Varianten allokieren aber selbst passenden Speicher;
	dann ist s der Rückgabewert.

	NACHTRAG:
	  In der ersten veröffentlichten Fassung war die Speicherverwaltung noch 
	  nicht korrekt. Insbesondere wurde der für die Stringeingabe allokierte
	  Speicher nicht freigegeben; dies ist nun ergänzt worden. Außerdem wird
	  jetzt bei jeder Allokation geprüft, ob diese erfolgreich war (d.h. ob
	  kein Nullzeiger zurückgegeben wurde). Auch dies sollte bei jedem 
	  ernsthaften Projekt Pflicht sein.
	  Entsprechende Kommentare wurden bei den Eingabefunktionen input5()
	  und input6() sowie bei der main-Funktion hinzugefügt.
*/

/*
	erste Fassung: benutzt das altvertraute scanf()

	Nachteile:
	 1.	scanf() liest nur bis zum nächsten Whitespace (z.B. Leerzeichen).
	 2.	scanf() kann keine eingeschränkten Puffergrößen berücksichtigen, 
		d.h. eine zu lange Eingabe schreibt über den vorgesehenen
		Speicherbereich hinaus!
	 3. Eingabe eines leeren Strings nicht möglich, weil von scanf() ignoriert
*/
void input1(char *s) {
	scanf("%s", s);
}

/*
	zweite Fassung: scanf() mit Längenangabe im Formatstring

	Vorteile:
	  - Nachteil 2 der ersten Lösung wird behoben; von langen Eingaben
		werden nur die ersten n (hier: 9) Zeichen gelesen, der Rest geht 
		in die nächste Eingabe ein.
	Nachteile:
	  - Nachteile 1 und 3 der ersten Fassung bleiben bestehen
	  - Die Größenangabe im Formatstring muß fest angegeben werden
	    (keine Variablen verwendbar) und muß zur Stringgröße passen
		(minus 1 für das Nullzeichen am Ende)
*/
void input2(char *s) {
	scanf("%9s", s);
}

/*
	dritte Fassung: verwendet gets() statt scanf()
	 
	Vorteile:
	  -	behebt die Nachteile 1 und 3 der ersten Lösung, liest immer 
		eine ganze Zeile bis zum Return; diese kann auch leer sein
	Nachteile:
	  - Nachteil 2 der ersten Fassung bleibt bestehen bzw. kehrt zurück:
		lange Eingaben sprengen den Puffer!
*/
void input3(char *s) {
	gets(s);
}

/*
	vierte Fassung: verwendet fgets() statt gets()

	Erläuterung: fgets() arbeitet wie gets(), mit zwei Unterschieden:
	  -	es liest eigentlich aus Dateien; aber die Standardeingabe
		(= Tastatur) kann als spezielle "Datei" stdin angesehen werden
	  -	man kann eine Puffergröße (= maximale Stringlänge) angeben -
		darauf kommt es uns hier an

	Vorteile:
	  - endlich sind alle drei Nachteile der ersten Fassung behoben:
	    es können ganze Zeilen bis zum Return gelesen werden, diese
		dürfen auch leer sein, und lange Eingaben werden abgefangen.
	  - im Gegensatz zur zweiten Version kann die Stringlänge auch 
		als Variable übergeben werden
	Nachteile:
	  - der Rest der Eingabe (der über die Maximallänge hinausgeht)
	    kann nicht mit eingelesen werden. Er bleibt für die nächste 
		Eingabe stehen; man könnte ihn auch mit fflush(stdin) leeren
		(was aber nicht standardisiert ist) oder in einen Dummy-String
		einlesen und verwerfen. So oder so ist die in den String 
		gelesene Eingabe in der Länge begrenzt, was je nach Anwendung 
		u.U. nicht optimal ist
	  - fgets() schreibt das \n am Zeilenende mit in den String (gets()
	    und scanf() hatten dieses entfernt); das ist lästig und müßte
		nachträglich entfernt werden (hier nicht ausgeführt)
*/
void input4(char *s, int l) {
	fgets(s, l, stdin);
}

/*
	fünfte Fassung: puffert lange Eingaben in "großem" String und 
	kürzt diesen nach Bedarf

	Erläuterung: Der String wird mit gets() in ein lokales Array
	  geschrieben, das i.a. groß genug sein sollte. Anschließend
	  wird genau die richtige Menge an Speicherplatz allokiert und
	  der eingegebene Speicherplatz hineinkopiert. Der Zeiger auf
	  diesen Speicherplatz wird an die aufrufende Funktion zurückgegeben.

	Vorteile:
	  - die meisten langen Eingaben können nichts kaputtmachen
	  - es sind fast beliebig lange Eingaben möglich
	  - trotzdem wird nur so viel Speicher dauerhaft belegt wie
	    unbedingt notwendig
	Nachteile:
	  - SEHR lange Eingaben könnten immer noch den Puffer überlaufen 
	    lassen; dies ist auch die Grundlage für sog. Buffer-Overflow-
		Angriffe durch Hacker

	NACHTRAG:
	  Der von malloc() zurückgegebene Zeiger wird jetzt auf NULL geprüft,
	  um eine möglicherweise fehlgeschlagene Allokation abzufangen.
*/
char *input5() {
	char puffer[100];
	char *s;
	gets(puffer);
	s = malloc(strlen(puffer)+1);
	if (s==NULL) {
		printf("\n\nFEHLER!!\n");
		printf("Es konnte kein Speicher allokiert werden!\n");
		printf("Das Programm wird beendet.\n\n\n");
		exit(1);
	}
	strcpy(s, puffer);
	return s;
}

/*
	sechste und letzte Fassung: absolut sichere Eingabe für 
	beliebig lange Strings

	Erläuterung:
	  Der Speicher für den String wird von vornherein dynamisch allokiert
	  (zunächst 10 Zeichen) weil jedes statische Array im Ernstfall zu 
	  klein sein könnte. Mit getchar() wird jeweils 1 Zeichen gelesen und
	  in den String geschrieben. Ist der Puffer voll, wird er mit realloc()
	  vergrößert (hier: jeweils verdoppelt); das kann beliebig oft passieren.
	  Dieser Vorgang bricht ab, wenn ein "Return" (\n) gelesen wird; danach
	  wird noch das Nullzeichen ans Stringende gesetzt und der Puffer auf
	  die minimal benötigte Länge zurechtgestutzt (wieder mit realloc()).
	Anmerkung:
	  Die Funktion getchar() gibt keinen char-Wert, sondern einen int zurück.
	  So kann sie auch besondere Rückgabewerte wie EOF ("end of file" =
	  Dateiende) haben, die keine Zeichen aus dem ASCII-Vorrat sind. Wenn 
	  es sich wie hier nur um einfache Zeichen handelt, müssen diese nach
	  char gecastet werden.

	Vorteile:
	  - alle obigen Fehlerquellen sind beseitigt
	Nachteile:
	  - verlängerte Laufzeit durch mehrfache Speicherallokation und 
		eigenhändige Implementierung anstelle optimierter Systemfunktionen.
		Ersteres ließe sich verbessern durch Optimierung der Anfangs-
		Puffergröße und des Vergrößerungsfaktors.

	NACHTRAG:
	  Der von malloc() bzw. realloc() zurückgegebene Zeiger wird jetzt auf 
	  NULL geprüft, um eine möglicherweise fehlgeschlagene Allokation 
	  abzufangen.
*/
char *input6() {
	char *s = 0;
	int c;         // puffert ein einzelnes Zeichen
	long p = 10;   // Puffergröße
	long l = 0;    // Stringlänge
	s = malloc(p);
	if (s==NULL) {
		printf("\n\nFEHLER!!\n");
		printf("Es konnte kein Speicher allokiert werden!\n");
		printf("Das Programm wird beendet.\n\n\n");
		exit(1);
	}
	do {
		c = (char) getchar();
		if (c == '\n')
			break;
		s[l] = c;
		l++;
		if (l == p) {
			// Puffer vergrößern (verdoppeln)
			p *= 2;
			s = realloc(s, p);
			//printf("...Puffer vergroessert auf %d Byte\n", p);
			if (s==NULL) {
				printf("\n\nFEHLER!!\n");
				printf("Es konnte kein Speicher allokiert werden!\n");
				printf("Das Programm wird beendet.\n\n\n");
				exit(1);
			}
		}
	} while(1);
	s[l] = '\0';
	s = realloc(s, l+1);
	//printf("...Puffer verkleinert auf %d Byte\n", l+1);
	if (s==NULL) {
		printf("\n\nFEHLER!!\n");
		printf("Es konnte kein Speicher allokiert werden!\n");
		printf("Das Programm wird beendet.\n\n\n");
		exit(1);
	}
	return s;
}


/*
	Nachtrag:
	  Die Hauptfunktion ruft in einer Schleife mehrmals die gewünschte 
	  Eingabefunktion auf. Bei den Varianten 5 und 6 muß auf die korrekte
	  Speicherverwaltung geachtet werden: Da die Eingabefunktionen jedesmal
	  neuen Speicher für die eingegebenen Strings allokieren und diesen
	  natürlich nicht selbst freigeben können, muß sich die main-Funktion
	  um die Freigabe kümmern. Weil die Stringlänge ermittelt werden muß,
	  BEVOR der Speicher freigegeben wird, wird die Hilfsvariable len
	  eingeführt.
*/
main()
{
	char string1[10];
	char *string2;
	int i = 0;
	int len = 0;
	
	printf("Welche Variante haetten Sie denn gerne?\n");
	printf("1. einfaches scanf()\n");
	printf("2. scanf() mit Laengenangabe\n");
	printf("3. einfaches gets()\n");
	printf("4. fgets() mit Laengenangabe\n");
	printf("5. gets() mit grossem Puffer\n");
	printf("6. perfekte Eingabe mit automatischem Puffer\n");
	printf("7. perfekte Eingabe benutzen fuer Datumswerte\n");
	printf("Ihre Wahl: ");
	scanf("%d\n", &i);
	switch(i) {
	case 1 :
		do {
			input1(string1);
			printf("Ihre Eingabe lautet: %s.\n\n", string1);
		} while (strlen(string1));	// solange Stringlänge nicht 0
		break;
	case 2:
		do {
			input2(string1);
			printf("Ihre Eingabe lautet: %s.\n\n", string1);
		} while (strlen(string1));	// solange Stringlänge nicht 0
		break;
	case 3:
		do {
			input3(string1);
			printf("Ihre Eingabe lautet: %s.\n\n", string1);
		} while (strlen(string1));	// solange Stringlänge nicht 0
		break;
	case 4:
		do {
			input4(string1, 9);
			printf("Ihre Eingabe lautet: %s.\n\n", string1);
		} while (strlen(string1));	// solange Stringlänge nicht 0
		break;
	case 5:
		do {
			string2 = input5();
			printf("Ihre Eingabe lautet: %s.\n\n", string2);
			len = strlen(string2);
			free(string2);  // WICHTIG: Speicher freigeben!
		} while (len);	// solange Stringlänge nicht 0
		break;
	case 6:
		do {
			string2 = input6();
			printf("Ihre Eingabe lautet: %s.\n\n", string2);
			len = strlen(string2);
			free(string2);  // WICHTIG: Speicher freigeben!
		} while (len);	// solange Stringlänge nicht 0
		break;
	case 7:
		do {
			int tag, monat, jahr, test;
			string2 = input6();
			printf("Ihre Eingabe lautet: %s.\n", string2);
			test = sscanf(string2, "%d.%d.%d", &tag, &monat, &jahr);
			if (test==3)
				printf("   als Datum: %d.%d.%d\n\n", tag, monat, jahr);
			else
				printf("   kein gueltiges Datum!\n\n");
			len = strlen(string2);
			free(string2);  // WICHTIG: Speicher freigeben!
		} while (len);	// solange Stringlänge nicht 0
		break;
	}
}



